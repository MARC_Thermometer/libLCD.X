/*! \file 00readme.txt
 *
 * \brief Initial notes
 *
 * This file provides some overview comments about the library.
 *
 * \page 1intro Overview
 *
 * libLCD provides a set of basic routines for dealing with a standard,
 * HD44780 compatible LCD character display module.  Displays larger
 * than 8 characters by one line are assumed to have an HD4410 or
 * equivalent.  All modern LCD character display modules meet this
 * requirement.
 *
 * \section requirements Requirements
 *
 * The library presumes a PIC24 with the LCD data lines on the  high four
 * bits of PORTB, the LCD enable on RB11 and the register select on RB10.
 * R/W is presumed to be pulled down.  Synchronization with the display
 * is managed entirely through preprogrammed delays.
 *
 * \dot
digraph LCDconnections
{
        size="1.5,2.0";
	fontname="Helvetica-Bold";
	label="LCD Connections";	
	node [ shape="record" style=filled fillcolor=mintcream
	      fontname="Helvetica"];
	st1 [ label="{D7|D6|D5|D4|RS|RW|E}|{RB15|RB14|RB13|RB12|RB10|n/c|RB11}" ];
}
 * \enddot
 *
 * \section funs Functions
 *
 * Functions are provided for the basic capabilities such as
 * initializing the LCD, displaying a character and sending
 * commands.  Many other capabilities are implemented as
 * macros which conserves program space.
 *
 * The following external functions are provided
 *
 *  \li LCDinit - Initialize the LCD
 *  \li LCDletter - Send a letter to the LCD
 *  \li LCDcommand - Send a command to the LCD
 *  \li LCDputs - Send a null-terminated string to the LCD
 *  \li Delay_ms - Delay for a specified number of milliseconds
 *
 * And the following macros
 *  \li LCDclear - Clear the display
 *  \li LCDposition - Move the cursor to a specified position
 *  \li LCDhome - Move the cursor home
 *  \li LCDline2 - Move tie cursor to the beginning of line 2
 *  \li LCDright - Move the cursor to the right
 *  \li LCDleft - Move the cursor to the left
 *  \li LCDshift - Shift the display
 *  \li LCDshiftLeft - Shift the display left
 *  \li LCDshiftRight - Shift the display right
 *  \li LCDcursorOn - Turn the underline cursor on
 *  \li LCDcursorOff - Turn the underline cursor off
 *
 * The include file LCD.h provides these macros and prototypes for
 * the functions.
 *
 * In addition, there are functions used internally and not exposed
 * in LCD.h.  These functions, as well as the pins used for the LCD,
 * are defined in LCDinternal.h.  These functions are:
 *  \li LCDsend - send a byte to the LCD
 *  \li LCDpulseEnableBit - toggle the LCD enable line
 *  \li LCDbusy - wait for the LCD to complete
 *
 * \dot
digraph LCDlib
{
    size="3, 3";
	fontname="Helvetica-Bold";
	label="LCD library functions";
	node [ fontname="Droid Sans Bold" ];
	LCDpulseEnableBit [ style="filled" fillcolor="moccasin"
		 fontname="Liberation Sans Narrow"];
	LCDbusy [ style="filled" fillcolor="moccasin"
		 fontname="Liberation Sans Narrow"];
	LCDsend [ style="filled" fillcolor="moccasin"
		 fontname="Liberation Sans Narrow"];
	LCDbusy->Delay_ms;
	LCDcommand->LCDbusy;
	LCDcommand->LCDsend;
	LCDcommand->Delay_ms;
	LCDinit->Delay_ms;
	LCDinit->LCDcommand;
	LCDletter->LCDbusy;
	LCDletter->LCDsend;
	LCDputs->LCDletter;
	LCDsend->LCDpulseEnableBit;
}
 * \enddot
 *
 * \section xmp Examples
 *
 * LCDinit() must be called before any other functions.  To simply
 * display "Hello, world" on the LCD:
 * \code
 *    LCDinit();
 *    LCDputs("Hello, world");
 * \endcode
 * To display an 'X' on the fourth position of the second line:
 * \code
 *    LCDinit();
 *    LCDposition(0x44);
 *    LCDletter('X');
 * \endcode
 *
 * \author John J. McDonough, WB8RCR
 * \date December 28, 2014, 7:15 PM
 */
